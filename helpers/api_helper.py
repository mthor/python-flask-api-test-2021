import requests
import threading
from concurrent.futures import ThreadPoolExecutor


def get_check_v1(url):
    resource = "v1/check"
    r = requests.get(url + resource)
    return r


def get_requests_by_threads(the_function, url, workers = 10, calls = 100):
    threads = []
    with ThreadPoolExecutor(max_workers=workers) as executor:
        for n in range(0, calls):
            r = executor.submit(the_function, url)
            threads.append(r)       

    # Please note, these are Future objects
    return threads


def create_report_from_future_responses(responses):
    report = {}
    twos = 0
    fours = 0
    fives = 0
    
    # Remember Future responses get accessed with .result()
    for res in responses:
        if res.result().status_code == 200:
            twos+=1
        if res.result().status_code == 400:
            fours+=1
        if res.result().status_code == 500:
            fives+=1
    
    report = {
        'count': len(responses),
        200: twos,
        400: fours,
        500: fives
    }

    return report