## If it won't find the modules you can try this
# import os
# import inspect
# import sys
#
# current_path = os.path.abspath(inspect.getsourcefile(lambda: 0))
# current_dir = os.path.dirname(current_path)
# root_dir = os.path.join(current_dir, os.pardir)
# sys.path.append(root_dir)

# Local modules
from helpers.api_helper import get_check_v1, get_requests_by_threads, create_report_from_future_responses


# Change this to whatever your locally hosted app tells you
host = "http://192.168.0.2:5001/"
workers = 10
calls = 100


def test_check_endpoint_single():
    response = get_check_v1(host)
    print(f"\n{response.status_code}")
    assert response.status_code in [200, 400, 500]


def test_check_endpoint_threaded_part2():
    responses = get_requests_by_threads(get_check_v1, host, workers, calls)
    report = create_report_from_future_responses(responses)
    print(f"\n{report}")
    assert report['count'] == calls
    assert report[200]/calls > 0.65
    assert report[500]/calls < 0.20
