# Super Simple API with Multi-threaded test

# Requires
Python 3

# Steps
1) Create virtual environment:  `virtualenv venv` or `python -m virtualenv venv`
2) Go into virtual environment:  
   1) Windows:  `source venv/Scripts/activate`
   2) Unix   :  `source venv/bin/activate`
3) Install Flask and Flask-RESTful and Requests:  `pip install -r requirements.txt`
4) Start the API service:  `python api.py`
5) Run the tests:  `pytest -s`